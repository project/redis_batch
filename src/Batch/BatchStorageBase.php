<?php

namespace Drupal\redis_batch\Batch;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\redis\RedisPrefixTrait;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Base class for batch storage implementations.
 */
abstract class BatchStorageBase implements RedisBatchStorageInterface {

  protected const TTL = 864_000;

  protected const UPDATE_SCRIPT = "if redis.call('exists', KEYS[1]) == 1 then redis.call('hset', KEYS[1], 'batch', ARGV[1]) end";

  use RedisPrefixTrait;

  /**
   * Constructs a new BatchStorageBase object.
   */
  public function __construct(
    protected readonly mixed $client,
    protected readonly CsrfTokenGenerator $csrfToken,
    protected readonly SerializationInterface $serializer,
    protected readonly SessionInterface $session,
  ) {
    $this->setPrefix('drupal:batch:items');
  }

  /**
   * {@inheritdoc}
   */
  public function load($id): bool|array {
    $this->session->start();
    $hash = $this->doLoad($this->getPrefix() . ':' . $id);
    if (!$hash) {
      return FALSE;
    }
    if (!$this->csrfToken->validate($hash['token'], (string) $id)) {
      return FALSE;
    }

    /** @var array */
    return $this->serializer::decode($hash['batch']);
  }

  /**
   * Loads a batch by key.
   */
  abstract public function doLoad(string $key): false|array;

  /**
   * {@inheritdoc}
   */
  public function create(array $batch): void {
    $this->session->start();
    $this->doCreate(
      $this->getPrefix() . ':' . $batch['id'],
      $this->csrfToken->get($batch['id']),
      $this->serializer::encode($batch)
    );
  }

  /**
   * Creates and saves a batch by key.
   */
  abstract public function doCreate(string $key, string $token, string $batch): void;

  /**
   * {@inheritdoc}
   */
  public function update(array $batch): array {
    $this->doUpdate($this->getPrefix() . ':' . $batch['id'], $batch);
    return $batch;
  }

  /**
   * Updates a batch by key.
   */
  abstract public function doUpdate(string $key, array $batch): void;

  /**
   * {@inheritdoc}
   */
  public function delete($id): void {
    $this->doDelete($this->getPrefix() . ':' . $id);
  }

  /**
   * Deletes a batch by key.
   */
  abstract public function doDelete(string $key): void;

  /**
   * {@inheritdoc}
   */
  public function cleanup(): void {}

}
