<?php

namespace Drupal\redis_batch\Batch;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Batch\BatchStorageInterface;
use Drupal\redis\ClientFactory;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Defines the batch factory for the Redis backend.
 */
final class BatchStorageFactory {

  /**
   * Get the actual batch storage backend.
   *
   * @throws \Exception
   */
  public static function get(ClientFactory $clientFactory, CsrfTokenGenerator $csrfToken, SerializationInterface $serializer, SessionInterface $session): BatchStorageInterface {
    $clientName = $clientFactory::getClientName();

    return match ($clientName) {
      'PhpRedis' => new PhpRedisBatchStorage($clientFactory::getClient(), $csrfToken, $serializer, $session),
      'Predis' => new PredisBatchStorage($clientFactory::getClient(), $csrfToken, $serializer, $session),
      'Relay' => new RelayBatchStorage($clientFactory::getClient(), $csrfToken, $serializer, $session),
      default => throw new \Exception('Not implemented for ' . $clientName),
    };
  }

}
