<?php

namespace Drupal\redis_batch\Batch;

/**
 * PhpRedis batch storage backend.
 *
 * @property \Redis $client
 */
class PhpRedisBatchStorage extends BatchStorageBase {

  /**
   * {@inheritdoc}
   */
  public function doLoad(string $key): false|array {
    return $this->client->hGetAll($key);
  }

  /**
   * {@inheritdoc}
   */
  public function doCreate(string $key, string $token, string $batch): void {
    $pipe = $this->client->multi();
    $pipe->hMSet($key, [
      'token' => $token,
      'batch' => $batch,
    ]);
    $pipe->expire($key, self::TTL);
    $pipe->exec();
  }

  /**
   * {@inheritdoc}
   */
  public function doUpdate(string $key, array $batch): void {
    $this->client->eval(self::UPDATE_SCRIPT, [$key, $this->serializer::encode($batch)], 1);
  }

  /**
   * {@inheritdoc}
   */
  public function doDelete(string $key): void {
    $this->client->del($key);
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): int {
    return $this->client->incr('drupal:batch:counter');
  }

}
