<?php

namespace Drupal\redis_batch\Batch;

use Predis\Pipeline\Pipeline;

/**
 * Predis batch storage backend.
 *
 * @property \Predis\Client $client
 */
class PredisBatchStorage extends BatchStorageBase {

  /**
   * {@inheritdoc}
   */
  public function doLoad(string $key): false|array {
    return $this->client->hgetall($key);
  }

  /**
   * {@inheritdoc}
   */
  public function doCreate(string $key, string $token, string $batch): void {
    $this->client->pipeline(function (Pipeline $pipe) use ($key, $token, $batch) {
      $pipe->hmset($key, [
        'token' => $token,
        'batch' => $batch,
      ]);
      $pipe->expire($key, self::TTL);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function doUpdate(string $key, array $batch): void {
    $this->client->eval(self::UPDATE_SCRIPT, 1, $key, $this->serializer::encode($batch));
  }

  /**
   * {@inheritdoc}
   */
  public function doDelete(string $key): void {
    $this->client->del($key);
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): int {
    return $this->client->incr('drupal:batch:counter');
  }

}
