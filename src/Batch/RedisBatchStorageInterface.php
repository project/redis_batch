<?php

namespace Drupal\redis_batch\Batch;

use Drupal\Core\Batch\BatchStorageInterface;

/**
 * Defines the Redis batch storage interface.
 */
interface RedisBatchStorageInterface extends BatchStorageInterface {

  /**
   * Returns a new batch id.
   *
   * @return int
   *   A batch id.
   */
  public function getId(): int;

}
