<?php

namespace Drupal\redis_batch\Batch;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Access\CsrfTokenGenerator;
use Relay\Relay;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Relay batch storage backend.
 *
 * @property \Relay\Relay $client
 */
class RelayBatchStorage extends PhpRedisBatchStorage {

  /**
   * {@inheritdoc}
   */
  public function __construct(mixed $client, CsrfTokenGenerator $csrfToken, SerializationInterface $serializer, SessionInterface $session) {
    parent::__construct($client, $csrfToken, $serializer, $session);
    $this->client->addIgnorePatterns('drupal:batch:*');
  }

  /**
   * {@inheritdoc}
   */
  public function doCreate(string $key, string $token, string $batch): void {
    $pipe = $this->client->multi();
    if (!$pipe instanceof Relay) {
      trigger_error('Unable to start pipeline to write batch', E_USER_WARNING);
      return;
    }

    $pipe->hMSet($key, [
      'token' => $token,
      'batch' => $batch,
    ]);
    $pipe->expire($key, self::TTL);
    $pipe->exec();
  }

}
