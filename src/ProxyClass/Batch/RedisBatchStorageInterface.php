<?php
// phpcs:ignoreFile

/**
 * This file was generated via php core/scripts/generate-proxy-class.php 'Drupal\redis_batch\Batch\RedisBatchStorageInterface' "modules/custom/redis_batch/src".
 */

namespace Drupal\redis_batch\ProxyClass\Batch {

    /**
     * Provides a proxy class for \Drupal\redis_batch\Batch\RedisBatchStorageInterface.
     *
     * @see \Drupal\Component\ProxyBuilder
     */
    class RedisBatchStorageInterface implements \Drupal\Core\Batch\BatchStorageInterface
    {

        use \Drupal\Core\DependencyInjection\DependencySerializationTrait;

        /**
         * The id of the original proxied service.
         *
         * @var string
         */
        protected $drupalProxyOriginalServiceId;

        /**
         * The real proxied service, after it was lazy loaded.
         *
         * @var \Drupal\redis_batch\Batch\RedisBatchStorageInterface
         */
        protected $service;

        /**
         * The service container.
         *
         * @var \Symfony\Component\DependencyInjection\ContainerInterface
         */
        protected $container;

        /**
         * Constructs a ProxyClass Drupal proxy object.
         *
         * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
         *   The container.
         * @param string $drupal_proxy_original_service_id
         *   The service ID of the original service.
         */
        public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $drupal_proxy_original_service_id)
        {
            $this->container = $container;
            $this->drupalProxyOriginalServiceId = $drupal_proxy_original_service_id;
        }

        /**
         * Lazy loads the real service from the container.
         *
         * @return object
         *   Returns the constructed real service.
         */
        protected function lazyLoadItself()
        {
            if (!isset($this->service)) {
                $this->service = $this->container->get($this->drupalProxyOriginalServiceId);
            }

            return $this->service;
        }

        /**
         * {@inheritdoc}
         */
        public function getId(): int
        {
            return $this->lazyLoadItself()->getId();
        }

        /**
         * {@inheritdoc}
         */
        public function load($id)
        {
            return $this->lazyLoadItself()->load($id);
        }

        /**
         * {@inheritdoc}
         */
        public function create(array $batch)
        {
            return $this->lazyLoadItself()->create($batch);
        }

        /**
         * {@inheritdoc}
         */
        public function update(array $batch)
        {
            return $this->lazyLoadItself()->update($batch);
        }

        /**
         * {@inheritdoc}
         */
        public function delete($id)
        {
            return $this->lazyLoadItself()->delete($id);
        }

        /**
         * {@inheritdoc}
         */
        public function cleanup()
        {
            return $this->lazyLoadItself()->cleanup();
        }

    }

}
