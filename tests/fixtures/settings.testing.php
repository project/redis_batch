<?php

// phpcs:ignoreFile

$settings['redis.connection'] = [
  'interface' => getenv('REDIS_INTERFACE'),
  'host' => getenv('REDIS_HOST') ?? 'redis',
];
