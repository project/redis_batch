<?php

namespace Drupal\Tests\redis_batch\Functional;

use Drupal\Tests\system\Functional\Batch\PageTest;

/**
 * Tests the content of the progress page using redis backend.
 *
 * @group redis_batch
 */
class RedisPageTest extends PageTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['batch_test', 'redis', 'redis_batch', 'redis_batch_test'];

}
