<?php

namespace Drupal\Tests\redis_batch\Functional;

use Drupal\Tests\redis\Traits\RedisTestInterfaceTrait;
use Drupal\Tests\system\Functional\Batch\ProcessingTest;

/**
 * Tests batch processing in form and non-form workflow using redis backend.
 *
 * @group redis_batch
 */
class RedisProcessingTest extends ProcessingTest {

  use RedisTestInterfaceTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['batch_test', 'redis', 'redis_batch', 'redis_batch_test'];

}
