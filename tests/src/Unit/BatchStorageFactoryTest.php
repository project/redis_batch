<?php

namespace Drupal\redis_batch\Batch;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\redis\Client\Predis;
use Drupal\redis\ClientFactory;
use Drupal\redis\ClientInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\TestTools\Random;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Batch storage factory tests.
 *
 * @group redis_batch
 */
class BatchStorageFactoryTest extends UnitTestCase {

  /**
   * Test get.
   *
   * @dataProvider getProvider
   */
  public function testGet(ClientInterface $client, ?string $expectedClass): void {
    ClientFactory::setClient($client);
    if (!$expectedClass) {
      $this->expectExceptionMessage('Not implemented for ' . ClientFactory::getClientName());
    }

    $storage = BatchStorageFactory::get(
      new ClientFactory(),
      $this->createMock(CsrfTokenGenerator::class),
      $this->createMock(SerializationInterface::class),
      $this->createMock(SessionInterface::class),
    );

    $this->assertEquals($expectedClass, $storage::class);
  }

  /**
   * Data provider for testGet().
   *
   * @return \Generator
   *   An array of client and expected class.
   */
  public function getProvider(): \Generator {
    $phpRedis = $this->createMock(ClientInterface::class);
    $phpRedis->method('getName')->willReturn('PhpRedis');
    yield 'PhpRedis' => [$phpRedis, PhpRedisBatchStorage::class];

    yield 'Predis' => [new Predis(), PredisBatchStorage::class];

    $relay = $this->createMock(ClientInterface::class);
    $relay->method('getName')->willReturn('Relay');
    $relay->method('getClient')->willReturn(new class() {

      /**
       * Adds ignore pattern(s). Matching keys will not be cached in memory.
       */
      public function addIgnorePatterns(string ...$pattern): void {}

    });
    yield 'Relay' => [$relay, RelayBatchStorage::class];

    $random = $this->createMock(ClientInterface::class);
    $random->method('getName')->willReturn(Random::machineName());
    yield 'Random' => [$random, NULL];
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    ClientFactory::reset();
  }

}
